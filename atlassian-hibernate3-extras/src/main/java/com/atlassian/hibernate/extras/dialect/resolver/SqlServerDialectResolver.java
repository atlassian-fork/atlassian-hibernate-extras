package com.atlassian.hibernate.extras.dialect.resolver;

import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.SQLServer2008Dialect;
import org.hibernate.dialect.SQLServerDialect;
import org.hibernate.dialect.resolver.AbstractDialectResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.DatabaseMetaData;
import java.sql.SQLException;

/**
 * Adds support for SQL Server 2012. The {@code StandardHibernateResolver} uses the {@code SQLServerDialect}, which is
 * configured for SQL Server 2000, when it is connected to SQL Server 2012 because its major version (11) is not in the
 * switch statement. SQL Server 2012 appears to work fine with SQL Server 2008's dialect, so this resolver returns that
 * dialect instead.
 *
 * @since 4.0
 */
public class SqlServerDialectResolver extends AbstractDialectResolver
{
    private static final Logger log = LoggerFactory.getLogger(SqlServerDialectResolver.class);

    /**
     * Handles SQL Server for major versions 8 (SQL Server 2000) through 11 (SQL Server 2012).
     *
     * @param metaData the database metadata, used to determine the database's product name and major version
     * @return an appropriate SQL Server dialect, or {@code null} if the connected DBMS is not SQL Server
     * @throws SQLException if retrieving database metadata fails
     */
    @Override
    public Dialect resolveDialectInternal(DatabaseMetaData metaData) throws SQLException
    {
        String databaseName = metaData.getDatabaseProductName();

        if (databaseName.startsWith("Microsoft SQL Server"))
        {
            int databaseMajorVersion = metaData.getDatabaseMajorVersion();
            switch (databaseMajorVersion)
            {
                case 8:
                    return createSqlServer2000Dialect(databaseName, databaseMajorVersion);
                case 9:
                    return createSqlServer2005Dialect(databaseName, databaseMajorVersion);
                case 10:
                    return createSqlServer2008Dialect(databaseName, databaseMajorVersion);
                case 11:
                    return createSqlServer2012Dialect(databaseName, databaseMajorVersion);
            }
        }

        //Hibernate registers its StandardDialectResolver as the lowest priority. If we return null from our resolver,
        //it will automatically fall back on that one. As a result, there's no point in adding the full gamut of DBMSs
        //to this resolver; we just fall back on the default one for everything else.
        log.trace("No dialect is known for {}; deferring to standard dialect resolver", databaseName);
        return null;
    }

    /**
     * Creates a {@code SQLServerDialect} suitable for use with SQL Server 2000.
     *
     * @param databaseName         the database product name
     * @param databaseMajorVersion the database major version
     * @return the created dialect
     */
    protected Dialect createSqlServer2000Dialect(String databaseName, int databaseMajorVersion)
    {
        log.debug("Returning SQL Server 2000 dialect for {} (Version: {})",
                databaseName, databaseMajorVersion);
        return new SQLServerDialect();
    }

    /**
     * Delegates to the {@link #createSqlServer2000Dialect(String, int) SQLServerDialect} as Hibernate does not have
     * a distinct dialect for SQL Server 2005 prior to Hibernate 3.6.
     *
     * @param databaseName         the database product name
     * @param databaseMajorVersion the database major version
     * @return the created dialect
     * @see #createSqlServer2000Dialect(String, int)
     */
    protected Dialect createSqlServer2005Dialect(String databaseName, int databaseMajorVersion)
    {
        //This is buggy, but to fix we must upgrade to Hibernate 3.6+
        return createSqlServer2000Dialect(databaseName, databaseMajorVersion);
    }

    /**
     * Creates a {@code SQLServer2008Dialect} suitable for use with SQL Server 2008.
     *
     * @param databaseName         the database product name
     * @param databaseMajorVersion the database major version
     * @return the created dialect
     */
    protected Dialect createSqlServer2008Dialect(String databaseName, int databaseMajorVersion)
    {
        log.debug("Returning SQL Server 2008 dialect for {} (Version: {})",
                databaseName, databaseMajorVersion);
        return new SQLServer2008Dialect();
    }

    /**
     * Delegates to the {@link #createSqlServer2008Dialect(String, int) SQLServer2008Dialect} as Hibernate does not
     * have a distinct dialect for SQL Server 2012.
     *
     * @param databaseName         the database product name
     * @param databaseMajorVersion the database major version
     * @return the created dialect
     * @see #createSqlServer2008Dialect(String, int)
     */
    protected Dialect createSqlServer2012Dialect(String databaseName, int databaseMajorVersion)
    {
        //SQL Server 2012 is more like the 2008 dialect than the 2000 dialect, which is what Hibernate's
        //StandardDialectResolver returns for it
        return createSqlServer2008Dialect(databaseName, databaseMajorVersion);
    }
}
