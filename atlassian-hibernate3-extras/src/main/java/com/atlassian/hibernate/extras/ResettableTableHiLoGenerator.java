package com.atlassian.hibernate.extras;

import org.hibernate.HibernateException;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.SessionImplementor;
import org.hibernate.id.IdentifierGeneratorHelper;
import org.hibernate.id.TableGenerator;
import org.hibernate.type.Type;
import org.hibernate.util.PropertiesHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Properties;

/**
 * An implementation of a resettable hi/lo {@code IdentifierGenerator} which is compatible with Hibernate 3.
 * <blockquote>
 * <b>hilo</b><br>
 * <br>
 * An <tt>IdentifierGenerator</tt> that returns a <tt>Long</tt>, constructed using
 * a hi/lo algorithm. The hi value MUST be fetched in a separate transaction
 * to the <tt>Session</tt> transaction so the generator must be able to obtain
 * a new connection and commit it. Hence this implementation may not
 * be used  when the user is supplying connections. In this
 * case a <tt>SequenceHiLoGenerator</tt> would be a better choice (where
 * supported).<br>
 * <br>
 * Mapping parameters supported: table, column, max_lo
 * </blockquote>
 */
//IMPLEMENTATION NOTE: This class is identical to the one in the hibernate-3.6 module, because it truly is compatible
//                     with Hibernate 3. If you make changes here, they should be duplicated there as well.
public class ResettableTableHiLoGenerator extends TableGenerator {

    /**
     * The max_lo parameter
     */
    public static final String MAX_LO = "max_lo";
    
    private static final Logger log = LoggerFactory.getLogger(ResettableTableHiLoGenerator.class);

    private long hi;
    private int lo;
    private int maxLo;
    private Class<?> returnClass;

    public void configure(Type type, Properties params, Dialect d) {
        super.configure(type, params, d);

        maxLo = PropertiesHelper.getInt(MAX_LO, params, Short.MAX_VALUE);
        lo = maxLo + 1; // so we "clock over" on the first invocation
        returnClass = type.getReturnedClass();
    }

    @SuppressWarnings("deprecation")
    public synchronized Serializable generate(SessionImplementor session, Object obj) throws HibernateException {
        if (lo > maxLo) {
            long hival = ((Number) super.generate(session, obj)).longValue();
            lo = 1;
            hi = hival * (maxLo + 1);
            log.debug("new hi value: " + hival);
        }

        return IdentifierGeneratorHelper.createNumber(hi + lo++, returnClass);
    }

    /**
     * Get the max_lo value, so that we can calculate a valid value for the next_hi DB value,
     * given the ids already allocated in the database.
     */
    public int getMaxLo() {
        return maxLo;
    }

    /**
     * Set the lo value to more than maxLo to force a reset of the hi value.
     */
    public synchronized void reset() {
        lo = maxLo + 1;
    }

    @Override
    public String[] sqlCreateStrings(Dialect dialect) {
        String[] createTableStatements = super.sqlCreateStrings(dialect);

        for (int i = 0; i < createTableStatements.length; i++) {
            String createTableStatement = createTableStatements[i];
            if (createTableStatement.startsWith(dialect.getCreateTableString())) {
                createTableStatements[i] = createTableStatement + dialect.getTableTypeString();
                break;
            }
        }

        return createTableStatements;
    }
}
