package com.atlassian.hibernate.extras.type;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * This class will convert NULL values retrieved from the database into EMPTY_STRING. This is required since Oracle
 * treats EMPTY_STRING as NULL. This class will not change how values are stored into the database.
 * <p>
 * Code was based on: https://www.hibernate.org/169.html (2nd example). For more details on Oracle and its handling
 * for empty strings, see http://download-west.oracle.com/docs/cd/B12037_01/server.101/b10759/sql_elements005.htm.
 */
@SuppressWarnings({"deprecation", "unused"})
public class EmptyStringUserType implements Serializable, UserType
{
    private static final String EMPTY_STRING = "";
    private static final int[] TYPES = {Types.VARCHAR};

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException
    {
        return deepCopy(cached);
    }

    @Override
    public Object deepCopy(Object value)
    {
        return value;
    }

    @Override
    public Serializable disassemble(Object value)
    {
        return (Serializable) deepCopy(value);
    }

    @Override
    public boolean equals(Object x, Object y)
    {
        return x == y || !(x == null || y == null) && x.equals(y);
    }

    /* (non-Javadoc)
     *  (at) see org (dot) hibernate.usertype.UserType#hashCode(java.lang.Object)
     */
    @Override
    public int hashCode(Object x) throws HibernateException
    {
        return x.hashCode();
    }

    @Override
    public boolean isMutable()
    {
        return false;
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, Object owner) throws HibernateException, SQLException
    {
        Object dbValue = Hibernate.STRING.nullSafeGet(rs, names[0]);
        if (dbValue != null)
        {
            return dbValue;
        }
        else
        {
            // Special case to handle Oracle databases:
            // If the attribute value retrieved is NULL, return EMPTY_STRING.
            return EMPTY_STRING;
        }
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index) throws HibernateException, SQLException
    {
        Hibernate.STRING.nullSafeSet(st, value, index);
    }

    /* (non-Javadoc)
     *  (at) see org (dot) hibernate.usertype.UserType#replace(java.lang.Object, java.lang.Object, java.lang.Object)
     */
    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException
    {
        return deepCopy(original);
    }

    @Override
    public Class returnedClass()
    {
        return String.class;
    }

    @Override
    public int[] sqlTypes()
    {
        return TYPES;
    }
}
