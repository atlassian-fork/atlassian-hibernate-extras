package com.atlassian.hibernate.extras.event;

import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.event.internal.DefaultMergeEventListener;
import org.hibernate.event.spi.MergeEvent;
import org.hibernate.persister.entity.EntityPersister;

import java.util.Map;

/**
 * Copy of IdTransferringMergeEventListener from Spring 3.1 with a fix for a {@code NullPointerException} that occurs
 * for entities with composite IDs.
 * <p>
 * Original documentation:
 * <blockquote>
 * <p>
 * Extension of Hibernate's DefaultMergeEventListener, transferring the ids of newly saved objects to the corresponding
 * original objects (that are part of the detached object graph passed into the <code>merge</code> method).
 * <p>
 * Transferring newly assigned ids to the original graph allows for continuing to use the original object graph,
 * despite merged copies being registered with the current Hibernate Session. This is particularly useful for web
 * applications that might want to store an object graph and then render it in a web view, with links that include
 * the id of certain (potentially newly saved) objects.
 * <p>
 * The merge behavior given by this MergeEventListener is nearly identical to TopLink's merge behavior. See PetClinic
 * for an example, which relies on ids being available for newly saved objects: the <code>HibernateClinic</code> and
 * <code>TopLinkClinic</code> DAO implementations both use straight merge calls, with the Hibernate SessionFactory
 * configuration specifying an <code>IdTransferringMergeEventListener</code>.
 * <p>
 * Typically specified as entry for LocalSessionFactoryBean's "eventListeners" map, with key "merge".
 * </blockquote>
 *
 * @since 6.1
 */
@SuppressWarnings("unused")
public class IdTransferringMergeEventListener extends DefaultMergeEventListener {

    /**
     * Hibernate 5.x implementation of ID transferal.
     */
    @Override
    protected void entityIsTransient(MergeEvent event, Map copyCache) {
        super.entityIsTransient(event, copyCache);

        SessionImplementor session = event.getSession();

        EntityPersister persister = session.getEntityPersister(event.getEntityName(), event.getEntity());
        // Extract ID from merged copy (which is currently registered with Session), and set the ID on the original
        // object (which remains detached).
        persister.setIdentifier(event.getOriginal(), persister.getIdentifier(event.getResult(), session), session);
    }
}
