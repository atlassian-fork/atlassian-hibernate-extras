package com.atlassian.hibernate.extras.hql;

import org.hibernate.boot.TempTableDdlTransactionHandling;
import org.hibernate.boot.spi.MetadataBuildingOptions;
import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.hql.internal.ast.HqlSqlWalker;
import org.hibernate.hql.internal.ast.tree.DeleteStatement;
import org.hibernate.hql.internal.ast.tree.FromElement;
import org.hibernate.hql.spi.id.IdTableSupport;
import org.hibernate.hql.spi.id.IdTableSupportStandardImpl;
import org.hibernate.hql.spi.id.local.AfterUseAction;
import org.hibernate.hql.spi.id.local.HelperAccessor;
import org.hibernate.hql.spi.id.local.IdTableInfoImpl;
import org.hibernate.hql.spi.id.local.LocalTemporaryTableBulkIdStrategy;
import org.hibernate.persister.entity.Queryable;

/**
 * An extension of Hibernate's {@code TemporaryTableBulkIdStrategy} that uses joins instead of sub-queries in
 * {@code where} clauses to delete against the temporary ID table.
 * <p>
 * To use this class you need to specify it as the implementation of {@code MultiTableBulkIdStrategy} while
 * configuring {@code SettingsFactory} for the Hibernate session about to be built.
 *
 * @see JoinTableBasedDeleteHandlerImpl
 * @since 6.1
 */
public class JoinTemporaryTableBulkIdStrategy extends LocalTemporaryTableBulkIdStrategy {

    private final AfterUseAction afterUseAction;

    private TempTableDdlTransactionHandling ddlTransactionHandling;

    public JoinTemporaryTableBulkIdStrategy() {
        //The construction perform here is drawn from MySQLDialect.getDefaultMultiTableBulkIdStrategy
        this(new IdTableSupportStandardImpl() {

                 @Override
                 public String getCreateIdTableCommand() {
                     return "create temporary table if not exists";
                 }

                 @Override
                 public String getDropIdTableCommand() {
                     return "drop temporary table";
                 }
             }, AfterUseAction.DROP, TempTableDdlTransactionHandling.NONE);
    }

    public JoinTemporaryTableBulkIdStrategy(IdTableSupport idTableSupport, AfterUseAction afterUseAction, TempTableDdlTransactionHandling ddlTransactionHandling) {
        super(idTableSupport, afterUseAction, ddlTransactionHandling);

        this.afterUseAction = afterUseAction;
        this.ddlTransactionHandling = ddlTransactionHandling;
    }

    @Override
    public DeleteHandler buildDeleteHandler(SessionFactoryImplementor factory, HqlSqlWalker walker) {
        DeleteStatement updateStatement = (DeleteStatement) walker.getAST();
        FromElement fromElement = updateStatement.getFromClause().getFromElement();
        Queryable targetedPersister = fromElement.getQueryable();
        IdTableInfoImpl tableInfo = getIdTableInfo(targetedPersister);

        return new JoinTableBasedDeleteHandlerImpl(factory, walker, tableInfo) {

            @Override
            protected void prepareForUse(Queryable persister, SharedSessionContractImplementor session) {
                HelperAccessor.createTempTable(tableInfo, ddlTransactionHandling, session);
            }

            @Override
            protected void releaseFromUse(Queryable persister, SharedSessionContractImplementor session) {
                HelperAccessor.releaseTempTable(tableInfo, afterUseAction, ddlTransactionHandling, session);
            }
        };
    }

    @Override
    protected void initialize(MetadataBuildingOptions buildingOptions, SessionFactoryOptions sessionFactoryOptions) {
        super.initialize(buildingOptions, sessionFactoryOptions);

        if (ddlTransactionHandling == null) {
            ddlTransactionHandling = sessionFactoryOptions.getTempTableDdlTransactionHandling();
        }
    }
}
