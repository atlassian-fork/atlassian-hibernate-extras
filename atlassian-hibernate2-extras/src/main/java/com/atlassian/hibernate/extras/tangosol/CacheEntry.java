package com.atlassian.hibernate.extras.tangosol;

public interface CacheEntry<K, V>
{
    void setValue(V value);

    V getValue();

    K getKey();
}
