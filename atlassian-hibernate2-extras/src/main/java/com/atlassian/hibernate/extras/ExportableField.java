package com.atlassian.hibernate.extras;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>This annotation can be used for any object's field that is not mapped by Hibernate.
 * In other words, it should not be added to fields that already mapped by Hibernate.
 * Otherwise, a WARN message will be printed out for each such field.</p>
 * <p>The annotation accepts a {@code type} parameter, which is a class.</p>
 * @since 6.2.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ExportableField {
    /**
     * The {@code Class} represented by this parameter must have a public no-argument constructor.
     */
    Class type();
}