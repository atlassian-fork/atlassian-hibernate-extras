package com.atlassian.hibernate.extras;

import net.sf.hibernate.Hibernate;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.Session;
import net.sf.hibernate.Transaction;
import net.sf.hibernate.collection.CollectionPersister;
import net.sf.hibernate.engine.SessionFactoryImplementor;
import net.sf.hibernate.persister.ClassPersister;
import net.sf.hibernate.proxy.HibernateProxy;
import net.sf.hibernate.proxy.HibernateProxyHelper;
import net.sf.hibernate.proxy.LazyInitializer;
import net.sf.hibernate.type.AbstractComponentType;
import net.sf.hibernate.type.BagType;
import net.sf.hibernate.type.BooleanType;
import net.sf.hibernate.type.DateType;
import net.sf.hibernate.type.ListType;
import net.sf.hibernate.type.MapType;
import net.sf.hibernate.type.PersistentCollectionType;
import net.sf.hibernate.type.SetType;
import net.sf.hibernate.type.TimestampType;
import net.sf.hibernate.type.Type;
import net.sf.hibernate.util.StringHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate.SessionFactoryUtils;

import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Locale.ENGLISH;

/**
 * Converts Hibernate data structures into an XML format used for backups.
 */
public abstract class XMLDatabinder
{
    private static final Logger log = LoggerFactory.getLogger(XMLDatabinder.class);

    public static final String ISO_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String ISO_DATE_FORMAT = "yyyy-MM-dd";

    //~ Instance variables ---------------------------------------------------------------------------------------------
    private SessionFactoryImplementor factory;


    // We use ExportHibernateHandle here rather than the Handle interface because we rely on the
    // getClass method.
    /**
     * Handles about to be or currently being iterated over by {@link #writeObjects(Writer, Iterable)}.
     */
    protected Set<ExportHibernateHandle> handles = new HashSet<>();

    /**
     * Handles of propertysetitems.  For some reason we do this last.  It may just be tradition.
     */
    protected Set<ExportHibernateHandle> bucketHandles = new HashSet<>();

    /**
     * A set of handles for identifying Objects to skip in the export.
     */
    protected Set<ExportHibernateHandle> excludedHandles = new HashSet<>();

    /**
     * A set of handles to be transferred to {@link #handles} after the current minor iteration of
     * {@link #writeObjects(Writer, Iterable)}.
     */
    protected Set<ExportHibernateHandle> nextHandles = new HashSet<>();

    /**
     * A set of handles of object that have already been written or otherwise dealt with.
     */
    protected Set<ExportHibernateHandle> processedHandles = new HashSet<>();

    protected String encoding;
    protected final HibernateTranslator translator;

    private DateFormat isoDateFormat = new SimpleDateFormat(ISO_DATE_FORMAT);
    private DateFormat isoTimestampFormat = new SimpleDateFormat(ISO_TIMESTAMP_FORMAT);

    /**
     * Gets initialised during {@link #toGenericXML(Writer, ExportProgress)},
     * so do not use it before then.
     */
    private ExportProgress progress;

    protected static final String LEFT_CHEVRON = "<";
    protected static final String RIGHT_CHEVRON = ">";
    protected static final String CARRIAGE_RETURN = "\n";
    protected static final String START_CLOSE_TAG = "</";
    protected static final String END_TAG_CARRIAGE_RETURN = RIGHT_CHEVRON + CARRIAGE_RETURN;
    private static final String CONST_NAME = "name";
    private static final String CONST_CLASS = "class";
    private static final String CONST_PACKAGE = "package";
    private static final String CONST_ENUM_CLASS = "enum-class";
    private static final String CONST_COMPOSITE_ELEMENT = "composite-element";
    private static final String CONST_ELEMENT = "element";
    private static final String CONST_SUBCOLLECTION = "subcollection";
    private static final String CONST_ID = "id";
    private static final String CONST_COMPOSITE_ID = "composite-id";
    private static final String CONST_OPEN_OBJECT_TAG = "<object";
    private static final String CONST_CLOSE_OBJECT_TAG = "</object>" + CARRIAGE_RETURN;
    private static final String CONST_COLLECTION = "collection";
    private static final String CONST_PROPERTY = "property";
    private static final String CONST_COMPONENT = "component";
    private static final String CONST_TYPE = "type";
    protected static final String CONST_OPEN_CDATA = "<![CDATA[";
    protected static final String CONST_CLOSE_CDATA = "]]>";

    private static final int BATCH_SIZE = Integer.parseInt(System.getProperty("export.space.batch.size", "300"));

    private Session session;
    private Transaction tx;

    /**
     * Objects of these class will not be exported.
     */
    private Collection<Class<?>> classesExcludedFromEntityExport = new ArrayList<>();
    /**
     * References to objects of these classes will not be exported.
     */
    private Collection<Class<?>> classesExcludedFromReferenceExport = new ArrayList<>();

    /*
     * An in-memory map that keeps all exportable fields of every class
     */
    private Map<String, List<FieldWithType>> exportableFieldsCache = new HashMap<>();

    //~ Constructors ---------------------------------------------------------------------------------------------------

    public XMLDatabinder(SessionFactoryImplementor factory, String encoding, HibernateTranslator translator)
    {
        this.factory = factory;
        this.encoding = encoding;
        this.translator = translator;
    }

    //~ Methods --------------------------------------------------------------------------------------------------------

    protected final ClassPersister getPersister(Class clazz) throws MappingException
    {
        return factory.getPersister(clazz);
    }

    protected final void objectWritten(ExportHibernateHandle handle) throws HibernateException
    {
        incrementProgress();
        processedHandles.add(handle);
    }

    /**
     * We need to commit and flush session for every batch of records handled
     */
    protected void incrementProgress() throws HibernateException
    {
        if (progress.increment() % BATCH_SIZE == 0)
        {
            commit();
        }
    }

    protected void incrementProgressTotal() {
        progress.incrementTotal();
    }

    protected void commit() throws HibernateException
    {
        session.flush();
        session.clear();
        tx.commit();
        tx = session.beginTransaction();
    }

    private void startTxn() throws HibernateException
    {
        session = SessionFactoryUtils.getSession(factory, true);
        tx = session.beginTransaction();
        commit();
    }

    public void toGenericXML(Writer writer, ExportProgress progressMeter) throws HibernateException, IOException {
        progress = progressMeter;
        startTxn();

        writer.write("<?xml version=\"1.0\" encoding=\"" + encoding + "\"?>\n");
        String date = Hibernate.TIMESTAMP.toString(new java.util.Date(), factory);
        writer.write("<hibernate-generic datetime=\"" + date + "\">\n");

        progress.setTotal(handles.size() + bucketHandles.size() + 2);

        progress.setStatus("Writing export");

        while (!handles.isEmpty())
        {
            writeObjects(writer, handles);
            handles = nextHandles;
            nextHandles = new HashSet<>();
        }
        writeObjects(writer, bucketHandles);

        writer.write("</hibernate-generic>");
        commit();
    }

    protected void writeObjects(Writer writer, Iterable<ExportHibernateHandle> iter) throws HibernateException, IOException {
        for (ExportHibernateHandle handle : iter) {
            if (isExcludedOrProcessed(handle)) {
                incrementProgress();
                continue;
            }

            Object object = translator.handleToObject(handle);
            object = maybeInitializeIfProxy(object);

            if (object != null) {
                ClassPersister persister = getPersister(object.getClass());
                writeObject(writer, handle, object, persister);
            } else {
                log.warn("Null object found for key: {}", handle.toString());
            }

            objectWritten(handle);
        }
    }

    protected final void writeObject(Writer writer, ExportHibernateHandle handle, Object object, ClassPersister persister) throws IOException, HibernateException {
        writer.write(CONST_OPEN_OBJECT_TAG);
        addClass(writer, object.getClass(), CONST_CLASS, CONST_PACKAGE);
        writer.write(END_TAG_CARRIAGE_RETURN);


        if (log.isDebugEnabled())
            log.debug("Writing object: {} with ID: {} to XML.", persister.getClassName(), persister.getIdentifier(object));

        //ID
        if (persister.hasIdentifierPropertyOrEmbeddedCompositeIdentifier()) {
            Object id = persister.getIdentifier(object);
            renderProperty(writer, persister.getIdentifierPropertyName(),
                    persister.getIdentifierType(), id, CONST_COMPOSITE_ID, CONST_ID, null, false);
        }

        //PROPERTIES
        Type[] types = persister.getPropertyTypes();
        Object[] values = persister.getPropertyValues(object);
        String[] names = persister.getPropertyNames();

        //This approach wont work for components + collections

        for (int i = 0; i < types.length; i++) {
            try {
                if (isExcludedAsProperty(types[i], values[i])) {
                    continue;
                }
            } catch (RuntimeException onfe) {
                // this is a reference to an object which doesn't exist, so don't try to back it up
                log.warn("Object doesn't exist for property {} of {}", names[i], handle.toString(), onfe.getCause());
                continue;
            }

            renderProperty(writer, names[i], types[i], values[i], CONST_COMPONENT, CONST_PROPERTY, CONST_COLLECTION, false);
        }

        List<String> propertyNames = Arrays.asList(names);
        Class clazz = object.getClass();
        for (FieldWithType fieldWithType : getExportableFields(clazz)) {
            Field field = fieldWithType.getField();

            String fieldName = field.getName();
            if (propertyNames.contains(fieldName)) {
                log.warn("The field {} annotated with ExportableField has been already serialized. Please remove the annotation.", fieldName);
                continue;
            }

            try {
                Object value = fieldWithType.getFieldGetter().invoke(object);
                renderOtherType(writer, fieldName, fieldWithType.getType(), value, CONST_PROPERTY, false);
            } catch (IllegalAccessException | InvocationTargetException e) {
                log.error("Cannot render {} of {}.", fieldName, object.getClass().getName(), e);
            }
        }

        writer.write(CONST_CLOSE_OBJECT_TAG);
    }

    /**
     * Get the exportable fields of a class from the in-memory map
     * If the list of exportable fields is not in the map, they will be extracted from the class with the Reflection API and then put into the map
     * @param clazz the class in question
     * @return the list of exportable fields of the input class
     */
    protected final List<FieldWithType> getExportableFields(Class<?> clazz) {
        return exportableFieldsCache.computeIfAbsent(clazz.getName(), className -> Arrays.stream(clazz.getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(ExportableField.class))
                .map(field -> {
                    ExportableField exportableFieldAnnotation = field.getAnnotation(ExportableField.class);
                    Object type = createInstance(exportableFieldAnnotation.type());
                    if (type == null) {
                        return null;
                    }
                    field.setAccessible(true);
                    Method fieldGetter;
                    try {
                        String prefix = type instanceof BooleanType ? "is" : "get";
                        fieldGetter = clazz.getMethod(prefix + capitalize(field.getName()));
                    } catch (NoSuchMethodException e) {
                        log.error("No getter method for property {} of {} found. This property will not be serialized to XML.", field.getName(), clazz.getName(), e);
                        return null;
                    }
                    return new FieldWithType(field, (Type) type, fieldGetter);
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
    }

    /**
     * An inner class that encapsulates an object's field and its corresponding Hibernate type
     */
    protected final class FieldWithType {
        private Field field;
        private Type type;
        private Method fieldGetter;

        private FieldWithType(Field field, Type type, Method fieldGetter) {
            this.field = field;
            this.type = type;
            this.fieldGetter = fieldGetter;
        }

        public Field getField() {
            return field;
        }

        public Type getType() {
            return type;
        }

        public Method getFieldGetter() {
            return fieldGetter;
        }
    }

    /**
     * Returns a String which capitalizes the first letter of the string.
     */
    private String capitalize(String name) {
        if (name == null || name.length() == 0) {
            return name;
        }
        return name.substring(0, 1).toUpperCase(ENGLISH) + name.substring(1);
    }

    /**
     * Instantiate an object by its class name
     * @param clazz the class whose the object belongs to
     * @return the object
     */
    private Object createInstance(Class clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            log.error("Cannot create a new object of {}.", clazz.getName(), e);
        }
        return null;
    }

    protected final boolean isExcludedOrProcessed(ExportHibernateHandle handle)
    {
        if (excludedHandles.contains(handle))
            return true;
        if (processedHandles.contains(handle))
            return true;
        Class handleClass = handle.getClazz();

        /* Enums aren't exported as entities. */
        if (handleClass.isEnum())
            return true;

        return classExtendsOneOf(handleClass, classesExcludedFromEntityExport);
    }

    /**
     * Returns true if clazz implements or extends any of the classes.
     */
    private boolean classExtendsOneOf(Class clazz, final Collection<Class<?>> classes)
    {
        for (Class<?> listedClass : classes)
        {
            if (listedClass.isAssignableFrom(clazz))
                return true;
        }
        return false;
    }

    protected final void addClass(Writer writer, Class clazz, String classAttributeName, String packageAttributeName) throws IOException
    {
        String className = clazz.getName();
        String unqualifiedClassName = StringHelper.unqualify(className);
        String packageName = StringHelper.qualifier(className);
        appendAttribute(writer, classAttributeName, unqualifiedClassName);
        appendAttribute(writer, packageAttributeName, packageName);
    }

    protected final Object maybeInitializeIfProxy(Object object)
    {
        if (!(object instanceof HibernateProxy))
        {
            return object;
        }

        LazyInitializer li = HibernateProxyHelper.getLazyInitializer((HibernateProxy) object);

        return li.getImplementation();
    }

    public XMLDatabinder bind(Object object)
    {
        handles.add(translator.objectOrHandleToHandle(object));
        return this;
    }

    public XMLDatabinder unbind(Object object)
    {
        excludedHandles.add(translator.objectOrHandleToHandle(object));
        return this;
    }

    /**
     * Register a set of objects as being ones that should be exported.
     * @param objects either objects or handles.
     * @return itself for chaining purposes
     */
    public XMLDatabinder bindAll(Collection objects)
    {
        objects.forEach(this::bind);
        return this;
    }

    /**
     * Register a set of objects as being ones that should *not* be exported.
     * @param objects either objects or handles.
     * @return itself for chaining purposes
     */
    public XMLDatabinder unbindAll(Collection objects)
    {
        objects.forEach(this::unbind);
        return this;
    }

    protected final void renderProperty(Writer writer, String name, Type type, Object value, String componentName, String propertyName,
                                String collectionName, boolean doType) throws HibernateException, IOException
    {
        if (type.isComponentType())
        {
            renderComponentType(writer, name, type, value, componentName);
        }
        else if (type.isPersistentCollectionType())
        {
            renderCollectionType(writer, name, type, value, collectionName);
        }
        else if (type.isEntityType())
        {
            renderEntityType(writer, name, value, propertyName);
            if (!isExcludedAsProperty(value))
                associatedObjectFound(value);
        }
        else if (type.getReturnedClass().isEnum())
        {
            renderEnumType(writer, name, type, value, propertyName);
        }
        else
        {
            renderOtherType(writer, name, type, value, propertyName, doType);
        }
    }

    protected final void renderOtherType(Writer writer, String name, Type type, Object value, String propertyName, boolean doType)
            throws HibernateException, IOException
    {
        writer.write(LEFT_CHEVRON + propertyName);
        if (name != null)
        {
            appendAttribute(writer, CONST_NAME, name);
        }
        if (doType)
        {
            appendAttribute(writer, CONST_TYPE, type.getName());
        }

        if (value != null)
        {
            writer.write(RIGHT_CHEVRON);

            String xmlValue = type.toString(value, factory);

            if (!parseCustomType(writer, type, value, xmlValue)) {
                if (type instanceof TimestampType)
                {
                    writer.write(isoTimestampFormat.format((java.util.Date) value));
                }
                else if (type instanceof DateType)
                {
                    writer.write(isoDateFormat.format((java.util.Date) value));
                }
                else
                {
                    writer.write(xmlValue);
                }
            }

            writer.write(START_CLOSE_TAG + propertyName + END_TAG_CARRIAGE_RETURN);
        }
        else
        {
            writer.write("/>");
        }
    }

    public abstract boolean parseCustomType(Writer writer, Type type, Object value, String xmlValue) throws IOException;

    protected final void renderEnumType(Writer writer, String name, Type type, Object value, String propertyName)
            throws HibernateException, IOException
    {
        writer.write(LEFT_CHEVRON + propertyName);
        if (name != null)
        {
            appendAttribute(writer, CONST_NAME, name);
        }

        addClass(writer, type.getReturnedClass(), CONST_ENUM_CLASS, CONST_PACKAGE);

        if (value != null)
        {
            writer.write(RIGHT_CHEVRON);

            String xmlValue = type.toString(value, factory);
            writer.write(xmlValue);
            writer.write(START_CLOSE_TAG + propertyName + END_TAG_CARRIAGE_RETURN);
        }
        else
        {
            writer.write("/>");
        }
    }


    private void appendAttribute(Writer writer, String attributeName, String attributeValue) throws IOException
    {
        writer.write(" " + attributeName + "=\"" + attributeValue + "\"");
    }

    protected final void renderEntityType(Writer writer, String name, Object value, String propertyName)
            throws HibernateException, IOException
    {
        if ((value = maybeInitializeIfProxy(value)) != null)
        {
            writer.write(LEFT_CHEVRON + propertyName);
            if (name != null)
            {
                appendAttribute(writer, CONST_NAME, name);
            }

            addClass(writer, value.getClass(), CONST_CLASS, CONST_PACKAGE);
            writer.write(RIGHT_CHEVRON);

            ClassPersister persister = getPersister(value.getClass());

            if (persister.hasIdentifierPropertyOrEmbeddedCompositeIdentifier())
            {
                Type idType = persister.getIdentifierType();
                Object id = persister.getIdentifier(value);
                renderProperty(writer, persister.getIdentifierPropertyName(), idType, id, CONST_COMPOSITE_ID, CONST_ID, null, false);
            }
            writer.write(START_CLOSE_TAG + propertyName + RIGHT_CHEVRON + CARRIAGE_RETURN);
        }
    }

    private void renderCollectionType(Writer writer, String name, Type type, Object value, String collectionName)
            throws HibernateException, IOException
    {

        PersistentCollectionType collectiontype = (PersistentCollectionType) type;
        String role = collectiontype.getRole();
        CollectionPersister persister = factory.getCollectionPersister(role);

        if (persister.isArray())
        {
            int length = Array.getLength(value);
            if (length == 0)
            {
                return;
            }
        }
        else if (value instanceof Collection)
        {
            // todo: If we checked the type of the collection for exclusion before doing the isEmpty check, which
            // at time of writing is where the collection gets initialized, then we'd save some time when doing
            // exports with types excluded.
            if (((Collection) value).isEmpty())
            {
                return;
            }
        }

        if (persister.isArray())
        {
            collectionName = "array";
        }

        writer.write(LEFT_CHEVRON + collectionName);
        if (name != null)
        {
            appendAttribute(writer, CONST_NAME, name);
        }
        if ((!persister.isArray()))
        {
            appendAttribute(writer, CONST_CLASS, type.getName());
        }

        Type elemType = persister.getElementType();
        writer.write(RIGHT_CHEVRON);

        if (persister.isArray())
        {
            int length = Array.getLength(value);

            for (int i = 0; i < length; i++)
            {
                Object element = Array.get(value, i);
                renderProperty(writer, null, elemType, element, CONST_COMPOSITE_ELEMENT,
                        CONST_ELEMENT, CONST_SUBCOLLECTION, true);
                associatedObjectFound(element);
            }
        }
        else
        {
            if (type instanceof ListType || (type instanceof SetType) || (type instanceof BagType))
            {
                for (Object collectionItem : ((Collection) value))
                {
                    if (isExcludedAsProperty(elemType, collectionItem))
                    {
                        continue;
                    }

                    renderProperty(writer, null, elemType, collectionItem, CONST_COMPOSITE_ELEMENT,
                            CONST_ELEMENT, CONST_SUBCOLLECTION, true);
                }
            }
            else if (type instanceof MapType)
            {
                for (Object o : ((Map) value).entrySet())
                {
                    Map.Entry e = (Map.Entry) o;
                    Object collectionKey = e.getKey();
                    Object collectionItem = e.getValue();

                    log.debug("Rendering map property: {} -> {}", collectionKey, collectionItem);
                    if (isExcludedAsProperty(elemType, collectionItem))
                    {
                        continue;
                    }

                    renderProperty(writer, collectionKey.toString(), elemType, collectionItem, CONST_COMPOSITE_ELEMENT,
                            CONST_ELEMENT, CONST_SUBCOLLECTION, true);
                }
            }
        }

        writer.write(START_CLOSE_TAG + collectionName + END_TAG_CARRIAGE_RETURN);
    }

    protected final boolean isExcludedAsProperty(Type type, Object obj)
    {
        return type.isEntityType() && type.isAssociationType() && isExcludedAsProperty(obj);
    }

    private boolean isExcludedAsProperty(Object obj)
    {
        if (obj == null)
        {
            return false;
        }

        if (obj instanceof Enum)
        {
            return false;
        }

        ExportHibernateHandle handle = translator.objectToHandle(obj);
        if (classExtendsOneOf(handle.getClazz(), classesExcludedFromReferenceExport))
            return true;
        return excludedHandles.contains(handle);
    }

    protected final void renderComponentType(Writer writer, String name, Type type, Object value, String componentName)
            throws HibernateException, IOException
    {
        if (value != null)
        {
            AbstractComponentType componenttype = (AbstractComponentType) type;
            writer.write(LEFT_CHEVRON + componentName);

            if (name != null)
            {
                appendAttribute(writer, CONST_NAME, name);
            }
            writer.write(RIGHT_CHEVRON);

            String[] properties = componenttype.getPropertyNames();
            Object[] subvalues = componenttype.getPropertyValues(value, null);//We know that null is okay here .. at least for ComponentType .... TODO: something safer??
            Type[] subtypes = componenttype.getSubtypes();

            for (int j = 0; j < properties.length; j++)
            {
                renderProperty(writer, properties[j], subtypes[j], subvalues[j], CONST_COMPONENT, CONST_PROPERTY,
                        CONST_COLLECTION, true);
            }

            writer.write(START_CLOSE_TAG + componentName + END_TAG_CARRIAGE_RETURN);
        }
    }

    protected final void associatedObjectFound(Object object)
    {
        if (object == null)
            return;

        object = maybeInitializeIfProxy(object);
        if (object != null)
        {
            ExportHibernateHandle handle = translator.objectToHandle(object);
            if (!isExcludedOrProcessed(handle))
            {
                boolean addedHandle = nextHandles.add(handle);
                if (addedHandle) {
                    incrementProgressTotal();
                }
            }
        }

    }

    /**
     * Mark a class as being excluded from the export.  Any entities of this type will be excluded from the
     * export.
     * @param clazz type to exclude from exports.
     */
    public void excludeClass(Class<?> clazz)
    {
        excludeClassFromEntityExport(clazz);
        excludeClassFromReferenceExport(clazz);
    }

    public void excludeClassFromReferenceExport(Class<?> clazz)
    {
        classesExcludedFromReferenceExport.add(clazz);
    }

    public void excludeClassFromEntityExport(Class<?> clazz)
    {
        classesExcludedFromEntityExport.add(clazz);
    }
}
